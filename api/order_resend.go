package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

// OrderResend 重新发送卡密短信
func OrderResend(ctx context.Context, client *sdk.Client, merchantId string, req *OrderResendReq) (*OrderResendResp, *ErrorResp, error) {

	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = OrderResendUrl
	request.SetContentType(requests.Json)
	request.JsonParams = utils.StructToMap(req)
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &OrderResendResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type OrderResendReq struct {
	OutTradeNo string `json:"outTradeNo"` // 商户侧订单号长度只能是1-64位
	OrderNo    string `json:"orderNo"`    // 平台订单号；与outTradeNo二选一。如果传了outTradeNo参数，则此参数无效
}

type OrderResendResp struct {
	IsSuccess bool `json:"isSuccess"`
}
