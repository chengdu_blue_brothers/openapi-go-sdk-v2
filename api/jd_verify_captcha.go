package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

// JDVerifyCaptcha 京东直冲短信验证码效验
func JDVerifyCaptcha(ctx context.Context, client *sdk.Client, merchantId string, req *JDVerifyCaptchaReq) (*JDVerifyCaptchaResp, *ErrorResp, error) {
	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = JdVerifyCaptchaUrl
	request.SetContentType(requests.Json)
	request.JsonParams = utils.StructToMap(req)
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &JDVerifyCaptchaResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type JDVerifyCaptchaReq struct {
	Mobile string `json:"mobile,omitempty"` // 手机号
	Jdcode string `json:"jdcode,omitempty"` // 京东验证码
}

type JDVerifyCaptchaResp struct {
	IsSuccess bool `json:"isSuccess"` // 是否成功
}
