package api

import (
	"context"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"testing"
)

func TestClient_OrderQuery(t *testing.T) {
	client, _ := sdk.NewClientWithAccessKey("5a39a17556a3a5bc9758d388855c000b", requests.HTTP, "192.168.6.13:9100")
	createReq := &OrderQueryReq{
		OutTradeNo: "1724720808",
	}
	query, errorResp, err := OrderQuery(context.Background(), client, "23378", createReq)
	if err != nil {
		fmt.Println(err)
	}
	if errorResp != nil {
		fmt.Println(errorResp)
	}
	fmt.Println(query)
	result, err := query.DecodeCard("5a39a17556a3a5bc9758d388855c000b")
	fmt.Println(result)
}
