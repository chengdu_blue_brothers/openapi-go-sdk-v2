package api

const (
	OrderCreateUrl     = "/openapi/v2/order/create"
	OrderQueryUrl      = "/openapi/v2/order/query"
	OrderResendUrl     = "/openapi/v2/order/resend"
	ProductQueryUrl    = "/openapi/v2/product/query"
	BalanceQueryUrl    = "/openapi/v2/balance/query"
	JdSendCaptchaUrl   = "/openapi/v2/jd/send"
	JdVerifyCaptchaUrl = "/openapi/v2/jd/verify"
)
const (
	BaseUrlProd    = "openapi.1688sup.com"     // 正式 URL
	BaseUrlSandbox = "test.openapi.1688sup.cn" // 沙箱 URL
)

type ErrorResp struct {
	Code     int         `json:"code"`
	Message  string      `json:"message"`
	Reason   string      `json:"reason"`
	Metadata interface{} `json:"metadata"`
}
