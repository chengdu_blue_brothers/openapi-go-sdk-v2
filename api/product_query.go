package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
)

// ProductQuery 获取商品
func ProductQuery(ctx context.Context, client *sdk.Client, merchantId string) (*ProductQueryResp, *ErrorResp, error) {

	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = ProductQueryUrl
	request.SetContentType(requests.Json)
	//request.JsonParams = map[string]interface{}{"1": 1}
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &ProductQueryResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type ProductQueryResp struct {
	Products []Product `json:"products"`
}
type Product struct {
	ProductId     string  `json:"productId"`
	ProductName   string  `json:"productName"`
	ProductType   string  `json:"productType"`
	FaceValue     float64 `json:"faceValue"`
	PurchasePrice float64 `json:"purchasePrice"`
	TemplateId    string  `json:"templateId"`
}
