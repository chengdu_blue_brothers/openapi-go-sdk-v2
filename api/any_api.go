package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
)

// AnyApi 通过原始的形式调用任意接口
// 当遇到sdk中没有封的接口时，使用此方法
// apiPath 为接口路径，如：/v1/product/query
// params 为接口参数，如：map[string]any{"productId": "123456"}
// 返回第一个参数为原始的JSON
func AnyApi(ctx context.Context, client *sdk.Client, merchantId, apiPath string, params map[string]any) (*string, *ErrorResp, error) {
	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = apiPath
	request.SetContentType(requests.Json)
	request.JsonParams = params
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	body := response.GetHttpContentBytes()
	bodyStr := string(body)
	return &bodyStr, nil, nil
}
