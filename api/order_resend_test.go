package api

import (
	"context"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"testing"
)

func TestOrderResend(t *testing.T) {
	client, _ := sdk.NewClientWithAccessKey("8db16e8cc8363ed4eb4c14f9520bcc32", requests.HTTP, "192.168.6.13:9100")
	createReq := &OrderResendReq{
		OutTradeNo: "1723536827",
	}
	OrderResend(context.Background(), client, "23329", createReq)
}
