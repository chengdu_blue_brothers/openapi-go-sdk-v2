package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

// JDSendCaptcha 京东直充短信验证码预发
func JDSendCaptcha(ctx context.Context, client *sdk.Client, merchantId string, req *JdSendCaptchaReq) (*JdSendCaptchaResp, *ErrorResp, error) {
	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = JdSendCaptchaUrl
	request.SetContentType(requests.Json)
	request.JsonParams = utils.StructToMap(req)
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &JdSendCaptchaResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type JdSendCaptchaReq struct {
	Mobile string `json:"mobile"` // 手机号
}

type JdSendCaptchaResp struct {
	IsSuccess bool `json:"isSuccess"` // 是否成功
}
