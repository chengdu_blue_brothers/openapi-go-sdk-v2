package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

// OrderCreate 下单接口
func OrderCreate(ctx context.Context, client *sdk.Client, merchantId string, req *OrderCreateReq) (*OrderCreateResp, *ErrorResp, error) {

	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = OrderCreateUrl
	request.SetContentType(requests.Json)
	request.JsonParams = utils.StructToMap(req)
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &OrderCreateResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type OrderCreateReq struct {
	OutTradeNo      string `json:"outTradeNo"`      // 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
	ProductId       string `json:"productId"`       // 商品编码
	Number          int    `json:"number"`          // 购买数量，目前只能固定是1
	NotifyUrl       string `json:"notifyUrl"`       // 异步通知地址
	RechargeAccount string `json:"rechargeAccount"` // 充值账号
	Extends         string `json:"extends"`         // 部分特殊商品需要，具体可联系商务
	Attach          string `json:"attach"`          //透传参数，当传了此参数时，查询和回调接口中将原样携带此参数
	UnitPrice       int64  `json:"unitPrice"`       //交易商品单价，单位：毫（1元等于10000亳） 0或不传表示不限制 当此价格小于采购价时，下单失败
	AccountType     int64  `json:"accountType"`     //账号类型：极少数特殊类型商品需要传此参数
}

type OrderCreateResp struct {
	OrderNo string `json:"orderNo"`
	Extra   string `json:"extra"`
}
