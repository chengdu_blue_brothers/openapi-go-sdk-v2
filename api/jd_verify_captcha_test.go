package api

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"testing"
)

func TestJdVerifyCaptcha(t *testing.T) {
	client, _ := sdk.NewClientWithAccessKey("5a39a17556a3a5bc9758d388855c000b", requests.HTTP, "211.137.105.198:17100")
	createReq := &JDVerifyCaptchaReq{
		Mobile: "18140154016",
		Jdcode: "123456",
	}
	marshal, _ := json.Marshal(createReq)
	fmt.Println(string(marshal))
	captcha, errorResp, err := JDVerifyCaptcha(context.Background(), client, "23378", createReq)
	fmt.Println(captcha, errorResp, err)
}
