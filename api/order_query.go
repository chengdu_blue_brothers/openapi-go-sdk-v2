package api

import (
	"context"
	"encoding/json"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

// OrderQuery 查询接口
func OrderQuery(ctx context.Context, client *sdk.Client, merchantId string, req *OrderQueryReq) (*OrderQueryResp, *ErrorResp, error) {

	request := requests.NewCommonRequest()
	request.MerchantId = merchantId
	request.Method = requests.POST
	request.ApiName = OrderQueryUrl
	request.SetContentType(requests.Json)
	request.JsonParams = utils.StructToMap(req)
	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		if _, ok := err.(*errors.ServerError); ok {
			errorResp := &ErrorResp{}
			err = json.Unmarshal(response.GetHttpContentBytes(), errorResp)
			return nil, errorResp, nil
		}
		return nil, nil, err
	}
	resp := &OrderQueryResp{}
	err = json.Unmarshal(response.GetHttpContentBytes(), resp)
	if err != nil {
		return nil, nil, err
	}
	return resp, nil, nil
}

type OrderQueryReq struct {
	OutTradeNo string `json:"outTradeNo"` // 合作商系统内部订单号,同一商户下不可重复， 同微信、支付宝的out_trade_no类似
}

type OrderQueryResp struct {
	TradeStatus     string  `json:"tradeStatus"`
	OrderNo         string  `json:"orderNo"`
	TradeStateDesc  string  `json:"tradeStateDesc"`
	Cards           []Cards `json:"cards"`
	MchId           int     `json:"mchId"`
	OutTradeNo      string  `json:"outTradeNo"`
	UnitPrice       float64 `json:"unitPrice"`
	Attach          string  `json:"attach"`
	Extra           string  `json:"extra"`
	RechargeAccount string  `json:"rechargeAccount"` // 充值账号
}
type Cards struct {
	No       string `json:"no"`
	Pwd      string `json:"pwd"`
	Deadline string `json:"deadline"`
	CardType int    `json:"cardType"`
}

// DecodeCard 解密卡密
func (c *OrderQueryResp) DecodeCard(secretKey string) (result map[string]string, err error) {
	if len(c.Cards) == 0 {
		return
	}
	result = map[string]string{}
	for _, card := range c.Cards {
		no, noerr := utils.GetDecodeCardText(card.No, secretKey)
		if noerr != nil {
			continue
		}
		pwd, pwderr := utils.GetDecodeCardText(card.Pwd, secretKey)
		if pwderr != nil {
			continue
		}
		result[no] = pwd
	}
	return
}
