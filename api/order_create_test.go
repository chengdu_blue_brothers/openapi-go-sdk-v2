package api

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"testing"
	"time"
)

func TestClient_OrderCreate(t *testing.T) {
	client, _ := sdk.NewClientWithAccessKey("5a39a17556a3a5bc9758d388855c000b", requests.HTTP, "192.168.6.13:9100")
	createReq := &OrderCreateReq{
		OutTradeNo: fmt.Sprintf("%d", time.Now().Unix()),
		ProductId:  "CBIUXVKI1785",
		Number:     1,
		//NotifyUrl:       "http://192.168.6.13:8100/api/notify/supplier",
		NotifyUrl: "http://192.168.6.184:8090/ping",
		//RechargeAccount: "18012345678",
		Extends:   "",
		Attach:    "{\"test\":\"test\"}",
		UnitPrice: 0,
		//AccountType:     1,
	}
	marshal, _ := json.Marshal(createReq)
	fmt.Println(string(marshal))
	OrderCreate(context.Background(), client, "23378", createReq)
}
