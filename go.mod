module gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2

require (
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af
	github.com/json-iterator/go v1.1.5
	github.com/modern-go/reflect2 v0.0.0-20180701023420-4b7aa43c6742
	github.com/stretchr/testify v1.3.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
)

go 1.21
