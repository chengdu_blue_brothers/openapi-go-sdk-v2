package notify

import (
	"encoding/json"
	"errors"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/auth"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/auth/credentials"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
	"io"
	"net/http"
)

type Notify struct {
	// merchantId 商户id
	merchantId string
	// secretKey 密钥key
	secretKey string
	signer    auth.Signer
}

func (n *Notify) GetMerchantId() string {
	return n.merchantId
}

func (n *Notify) GetSecretKey() string {
	return n.secretKey
}
func NewNotify(merchantId string, secretKey string) *Notify {

	n := &Notify{merchantId: merchantId, secretKey: secretKey}
	credential := &credentials.AccessKeyCredential{
		SecretKey: secretKey,
	}
	n.signer, _ = auth.NewSignerWithCredential(credential)

	return n
}

type OrderReq struct {
	TradeStatus     string  `json:"tradeStatus"`
	OrderNo         string  `json:"orderNo"`
	TradeStateDesc  string  `json:"tradeStateDesc"`
	MchId           int     `json:"mchId"`
	OutTradeNo      string  `json:"outTradeNo"`
	Cards           []Cards `json:"cards"`
	RechargeAccount string  `json:"rechargeAccount"`
	UnitPrice       float64 `json:"unitPrice"`
	Attach          string  `json:"attach"`
	Extra           string  `json:"extra"`
}

type Cards struct {
	No       string `json:"no"`
	Pwd      string `json:"pwd"`
	Deadline string `json:"deadline"`
	CardType int    `json:"cardType"`
	Url      string `json:"url"`
}

func (n *Notify) ParseAndVerify(req *http.Request) (*OrderReq, error) {
	var (
		authorization = req.Header.Get("Authorization")
	)
	fmt.Println("authorization:" + authorization)
	// 读取请求主体
	body, err := io.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}

	calcStrPre := fmt.Sprintf("%s%s", string(body), n.merchantId)
	sign := n.signer.Sign(calcStrPre)
	if sign != authorization {
		return nil, errors.New("签名错误")
	}
	// 解析JSON数据
	orderReq := &OrderReq{}
	err = json.Unmarshal(body, orderReq)
	if err != nil {
		return nil, err
	}
	return orderReq, nil
}

// DecodeCard 解密卡密
func (c *OrderReq) DecodeCard(secretKey string) (result map[string]string, err error) {
	if len(c.Cards) == 0 {
		return
	}
	result = map[string]string{}
	for _, card := range c.Cards {
		no, noerr := utils.GetDecodeCardText(card.No, secretKey)
		if noerr != nil {
			continue
		}
		pwd, pwderr := utils.GetDecodeCardText(card.Pwd, secretKey)
		if pwderr != nil {
			continue
		}
		result[no] = pwd
	}
	return
}
