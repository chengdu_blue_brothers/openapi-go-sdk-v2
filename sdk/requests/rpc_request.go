package requests

import (
	"bytes"
	"fmt"
	"io"
	"strings"

	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

type RpcRequest struct {
	*baseRequest
}

func (request *RpcRequest) init() {
	request.baseRequest = defaultBaseRequest()
	request.Method = POST
}

func (*RpcRequest) GetStyle() string {
	return RPC
}

func (request *RpcRequest) GetBodyReader() io.Reader {
	if request.FormParams != nil && len(request.FormParams) > 0 {
		formString := utils.GetUrlFormedMap(request.FormParams)
		request.SetContentType(Form)
		return strings.NewReader(formString)
	}
	if request.JsonParams != nil && len(request.JsonParams) > 0 {
		jsonMap := utils.GetJsonMap(request.JsonParams)
		request.SetContentType(Json)
		return bytes.NewBuffer(jsonMap)
	}
	return strings.NewReader("")
}

func (request *RpcRequest) BuildQueries() string {
	request.queries = "?" + utils.GetUrlFormedMap(request.QueryParams)
	return request.queries
}

func (request *RpcRequest) BuildUrl() string {
	url := fmt.Sprintf("%s://%s", strings.ToLower(request.Scheme), request.Domain)
	if len(request.Port) > 0 {
		url = fmt.Sprintf("%s:%s", url, request.Port)
	}
	return url + request.GetActionName() + request.BuildQueries()
}

func (request *RpcRequest) GetVersion() string {
	return request.version
}

func (request *RpcRequest) GetActionName() string {
	return request.actionName
}

func (request *RpcRequest) addPathParam(key, value string) {
	panic("not support")
}

func (request *RpcRequest) InitWithApiInfo(product, version, action, serviceCode, endpointType string) {
	request.init()
	request.product = product
	request.version = version
	request.actionName = action
	request.Headers["x-acs-version"] = version
	request.Headers["x-acs-action"] = action
}
