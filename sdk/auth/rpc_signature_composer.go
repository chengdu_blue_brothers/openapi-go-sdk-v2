package auth

import (
	"fmt"
	"time"

	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/utils"
)

var hookGetNonce = func(fn func() string) string {
	return fn()
}

func signRpcRequest(request requests.AcsRequest, signer Signer) (err error) {
	timestamp := time.Now().Unix()
	calcStrPre := buildRpcStringToSign(request, timestamp)
	signature := signer.Sign(calcStrPre)

	authorization := fmt.Sprintf("MD5 mchid=%s,timestamp=%d,signature=%s", request.GetMerchantId(), timestamp, signature)

	request.SetStringToSign(authorization)
	request.GetHeaders()["Authorization"] = authorization
	//request.GetHeaders()["Authorization"] = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MjMwOTU1NDMsImp0aSI6IjQ5MiIsIlVzZXJJZCI6NDkyLCJOaWNrTmFtZSI6IueUqOaIt18xMzIwMDAwMDAwMCIsIkF2YXRhciI6IiIsIk9wZW5JZCI6IiIsIlBob25lTnVtYmVyIjoiMTMyMDAwMDAwMDAiLCJDbGllbnRUeXBlIjoyLCJXeE9mZmljaWFsT3BlbklkIjoiIn0.N3dYPWCH8Ow4xYFiJdo4AtRKoqHIrLNVIrStosA_Ikg"

	return
}
func buildRpcStringToSign(request requests.AcsRequest, timestamp int64) (calcStrPre string) {
	body := ""
	if request.GetFormParams() != nil && len(request.GetFormParams()) > 0 {
		signParams := make(map[string]string)
		for key, value := range request.GetFormParams() {
			signParams[key] = value
		}
		body = utils.GetUrlFormedMap(signParams)
	}
	if request.GetJsonParams() != nil && len(request.GetJsonParams()) > 0 {
		signParams := make(map[string]interface{})
		for key, value := range request.GetJsonParams() {
			signParams[key] = value
		}
		body = string(utils.GetJsonMap(signParams))
	}
	calcStrPre = fmt.Sprintf("%s%s%d%s", request.GetActionName(), body, timestamp, request.GetMerchantId())
	return
}
