package auth

import (
	"fmt"
	"reflect"

	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/auth/credentials"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/auth/signers"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/errors"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
)

type Signer interface {
	GetType() string
	GetVersion() string
	GetAccessKeyId() (string, error)
	GetExtraParam() map[string]string
	Sign(calcStrPre string) string
}

func NewSignerWithCredential(credential Credential) (signer Signer, err error) {
	switch instance := credential.(type) {
	case *credentials.AccessKeyCredential:
		{
			signer = signers.NewAccessKeySigner(instance)
		}
	default:
		message := fmt.Sprintf(errors.UnsupportedCredentialErrorMessage, reflect.TypeOf(credential))
		err = errors.NewClientError(errors.UnsupportedCredentialErrorCode, message, nil)
	}
	return
}

func Sign(request requests.AcsRequest, signer Signer) (err error) {
	switch request.GetStyle() {
	case requests.RPC:
		{
			err = signRpcRequest(request, signer)
		}
	default:
		message := fmt.Sprintf(errors.UnknownRequestTypeErrorMessage, reflect.TypeOf(request))
		err = errors.NewClientError(errors.UnknownRequestTypeErrorCode, message, nil)
	}

	return
}
