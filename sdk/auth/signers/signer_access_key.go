package signers

import (
	"crypto/md5"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/auth/credentials"
)

type AccessKeySigner struct {
	credential *credentials.AccessKeyCredential
}

func (signer *AccessKeySigner) GetExtraParam() map[string]string {
	return nil
}

func NewAccessKeySigner(credential *credentials.AccessKeyCredential) *AccessKeySigner {
	return &AccessKeySigner{
		credential: credential,
	}
}

func (*AccessKeySigner) GetType() string {
	return ""
}

func (*AccessKeySigner) GetVersion() string {
	return "1.0"
}

func (signer *AccessKeySigner) GetAccessKeyId() (accessKeyId string, err error) {
	return signer.credential.SecretKey, nil
}

func (signer *AccessKeySigner) Sign(calcStrPre string) string {
	s := calcStrPre + signer.credential.SecretKey
	fmt.Println("sign before:" + s)
	srcCode := md5.Sum([]byte(s))
	code := fmt.Sprintf("%x", srcCode)
	fmt.Println("sign after:" + code)
	return code
}
