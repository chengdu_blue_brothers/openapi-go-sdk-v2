package credentials

type AccessKeyCredential struct {
	SecretKey string
}

func NewAccessKeyCredential(secretKey string) *AccessKeyCredential {
	return &AccessKeyCredential{
		SecretKey: secretKey,
	}
}
