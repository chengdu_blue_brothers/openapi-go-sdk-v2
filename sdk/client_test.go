package sdk

import (
	"encoding/json"
	"fmt"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/api"
	"gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/requests"
	"testing"
)

func TestNewClientWithAccessKey(t *testing.T) {
	client, _ := NewClientWithAccessKey("789", requests.HTTPS, api.BaseUrlSandbox)

	request := requests.NewCommonRequest()
	//request.MerchantId = "123"
	//request.Method = requests.POST
	//request.Scheme = requests.HTTPS
	//request.Domain = api.BaseUrlSandbox
	//
	//request.ApiName = api.OrderQuery
	//request.ApiName = api.OrderQuery
	//request.JsonParams["out_trade_no"] = "20240101111111"

	request.MerchantId = "123"
	request.Method = requests.GET
	request.Scheme = requests.HTTP
	request.Domain = "localhost:9600"

	request.ApiName = "/app/h5/v1/getCart"
	request.QueryParams["size"] = "10"

	response, err := client.ProcessCommonRequest(request)
	if err != nil {
		fmt.Println(err)
	}
	r := res{}
	json.Unmarshal(response.GetHttpContentBytes(), &r)
	fmt.Println(r)
}

type res struct {
	Code int    `json:"code"`
	Msg  string `json:"msg"`
	Data string `json:"data"`
}
