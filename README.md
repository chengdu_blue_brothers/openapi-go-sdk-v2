# 蓝色兄弟 openapi-v2
使用前请务必详细阅读本文档，本文档包含
1. 发起请求示例
2. 主动查询余额信息示例
3. 通知验签示例


## 发起请求示例
```go
    import (
        "gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/api"
        "gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk"
    )
    // 第一步：初使化client实例
    merchantId := "您的商户ID"
    secretKey := "您的商户密钥"
    url := "请求地址"
    client, err := sdk.NewClientWithAccessKey(secretKey, requests.HTTP, url)
    if err != nil {
        panic(err)
    }
    // 第二步：调用相关API接口：以查询订单为例
    query, errorResp, err := api.BalanceQuery(context.Background(), client, merchantId)
    if err!=nil{
        fmt.Println(err)
    }
    if errorResp!=nil{
        fmt.Println(errorResp)
    }
	fmt.Println(queryResp)
```


## 订单通知结果验签示例
```go
    import (
        "gitee.com/chengdu_blue_brothers/openapi-go-sdk-v2/sdk/notify"
    )
    // 第一步：初使化client实例
    client := notify.NewNotify(testMerchantId, testSecretKey)
    
    // 第二步：验签并解析结果
    req, err := client.ParseAndVerify(request)
    if err != nil {
        panic(err)
    }

```


## 解密卡密示例
```go
// 省略初使化client实例等步骤
// 验签并解析
orderReq, err := notify.ParseAndVerify(request)
if err != nil {
    panic(err)
}
// 解码
decodeCard, err := orderReq.DecodeCard(secretKey)
// decodeCard中的key为卡号，value为卡密(有些商品不存在卡密则为空)
```


## 更多示例可参考项目中openapi-go-sdk-v2/api文件夹"*_test.go"结尾的单元测试代码
